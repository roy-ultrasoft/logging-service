var httpLogger = new Object();
var clientIp = 0;

httpLogger.doLoggedHttpRequest = function(url, method) {
	 var req = new XMLHttpRequest();
	 req.loggingData = new LoggingData(url, "LOG_REQUESTS");
	 req.onreadystatechange = function() {
		 collectLoggingData(req);
	 }
	 req.open(method, url);
	 req.send();
};

function collectLoggingData(req) {
	req.loggingData.results["readyStateDate_" + req.readyState] = new Date().toISOString();
	if (req.readyState == 4) { 
		req.loggingData.results.statusCode = req.status;
		req.loggingData.results.browser = encodeURIComponent(navigator.appCodeName) + " " + encodeURIComponent(navigator.appVersion);
		req.loggingData.results.responseHeaders = req.getAllResponseHeaders();
		req.loggingData.results.originUrl = window.location.href;
		req.loggingData.results.clientIp = curentClientIp;
		doLog(req.loggingData);
	}
 }
	
function doLog(loggingData) {
	var req = new XMLHttpRequest();
	req.calledByDoLog = true;
	req.open("POST", "/logging-service/JsonLogServlet", true);
	req.send(JSON.stringify(loggingData));
}

class LoggingData {
	constructor(url, tableName) {
		this.logger = new Object();
		this.results = new Object();
		
		this.logger.tableName = tableName;
		this.results.url = url;
	}
}

//@see: https://ourcodeworld.com/articles/read/257/how-to-get-the-client-ip-address-with-javascript-only

var curentClientIp = "";
function searchCuretIP(onNewIP) { //  onNewIp - your listener function for new IPs
    //compatibility for firefox and chrome
    var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    var pc = new myPeerConnection({
        iceServers: []
    }),
    noop = function() {},
    localIPs = {},
    ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
    key;

    function iterateIP(ip) {
        if (!localIPs[ip]) onNewIP(ip);
        localIPs[ip] = true;
    }

     //create a bogus data channel
    pc.createDataChannel("");

    // create offer and set local description
    pc.createOffer().then(function(sdp) {
        sdp.sdp.split('\n').forEach(function(line) {
            if (line.indexOf('candidate') < 0) return;
            line.match(ipRegex).forEach(iterateIP);
        });
        
        pc.setLocalDescription(sdp, noop, noop);
    }).catch(function(reason) {
        // An error occurred, so handle the failure to connect
    });

    //listen for candidate events
    pc.onicecandidate = function(ice) {
        if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
        ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
    };
}

searchCuretIP(function(ip){
	curentClientIp = ip;
});
